var LocalStrategy   = require('passport-local').Strategy;
var User = require('../Model/users');
var bCrypt = require('bcrypt-nodejs');
var mailchimp = require('mailchimp-v3');

mailchimp.setApiKey(process.env.MAILCHIMP);

module.exports = function(passport){

    passport.use('signup', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {

            findOrCreateUser = function(){

                User.findOne({ 'username' :  username }, function(err, user) {
                    if (err){
                        return done(err);
                    }
                    
                    if (user) {
                        return done(null, false, req.flash('message','User Already Exists'));
                    } else {
                        var newUser = new User();

                        var batch = mailchimp.createBatch('lists/b150ab8e7d/members', 'POST');
                        batch
                            .add({
                                body: {
                                    status        : 'subscribed',
                                    email_address : username
                                }
                            })
                            .send()
                            .then(function(result){
                            })
                            .catch(function(error){
                                throw error;
                            });

                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.guest = false;

                        newUser.save(function(err) {
                            if (err){
                                throw err;
                            }
                            
                            return done(null, newUser);
                        });
                    }
                });
            };

            
            if (password == req.body.confirm){
                process.nextTick(findOrCreateUser);
            }else{
                return done(null, false, req.flash('message','Passwords don\'t match'));
            }
        })
    );

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }
}