var LocalStrategy   = require('passport-local').Strategy;
var User = require('../Model/users');
var bCrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");

module.exports = function(passport){

    passport.use('guest', new LocalStrategy({
        passReqToCallback : true
        },
        function(req, username, password, done) {

            var username = randomstring.generate();
            var password = randomstring.generate();

            findOrCreateUser = function(){
                var newUser = new User();

                newUser.username = username;
                newUser.password = createHash(password);
                newUser.guest = true;

                newUser.save(function(err) {
                    if (err){
                        throw err;
                    }

                    return done(null, newUser);
                });
            }

            process.nextTick(findOrCreateUser);
        })
    );

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }
}
