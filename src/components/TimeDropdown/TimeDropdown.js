/* eslint-disable no-shadow */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Select from 'react-select';
import { setFilterTime as setFilterTimeAction } from '../../actions/venues';
import { selectSearchFilter } from '../../reducers/venues';
import { TIME_OPTIONS } from '../../constants';
import s from '../dropdown.css';

class TimeDropdown extends React.Component {

  static propTypes = {
    className: PropTypes.string,
    curFilter: PropTypes.object,
    selectSearchFilter: PropTypes.func,
    setFilterTime: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.options = TIME_OPTIONS.map(option => {
      return {
        value: option.value,
        unit: option.unit,
        label: <p>{option.label}</p>,
      };
    });
    this.state = {
      selected: props.curFilter,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selected: nextProps.curFilter || this.state.selected,
    });
  }

  render() {
    const { className, setFilterTime, ...props } = this.props;
    return (
      <Select
        {...props}
        key={'time'}
        className={`${s.dropdown} ${className}`}
        clearable={false}
        searchable={false}
        options={this.options}
        value={this.state.selected}
        onChange={async (value) => {
          if (value !== this.state.selected) {
            await setFilterTime({ value });
            this.setState({ selected: value });
          }
        }}
        placeholder={'time'}
      />
    );
  }
}

const mapState = (state, props) => ({
  curFilter: selectSearchFilter(state, props).time,
});

const mapDispatch = {
  setFilterTime: setFilterTimeAction,
};

const EnhancedConent = connect(mapState, mapDispatch)(TimeDropdown);

export default withStyles(s)(EnhancedConent);
