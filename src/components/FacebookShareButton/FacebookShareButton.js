/**
 * Facebook share button component
 */

import React, { Component, PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from '../sharebutton.css';

class FacebookShareButton extends Component {
  static propTypes = {
    appId: PropTypes.string.isRequired,
    url: PropTypes.string,
    redirect_uri: PropTypes.string,
    quote: PropTypes.string,
  };

  render() {
    const { appId, url, redirect_uri, quote } = this.props;
    return (
      <a className={s.resp_sharing_button__link} href={`https://www.facebook.com/dialog/share?app_id=${appId}&href=${url}&quote=${quote}&redirect_uri=${redirect_uri}`}>
        <div className={`${s.resp_sharing_button} ${s.resp_sharing_button__facebook} ${s.resp_sharing_button__small}`}>
          <div aria-hidden="true" className={`${s.resp_sharing_button__icon} ${s.resp_sharing_button__icon__solid}`} >
            <svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24">
              <g>
                <path d="M18.768,7.465H14.5V5.56c0-0.896,0.594-1.105,1.012-1.105s2.988,0,2.988,0V0.513L14.171,0.5C10.244,0.5,9.5,3.438,9.5,5.32 v2.145h-3v4h3c0,5.212,0,12,0,12h5c0,0,0-6.85,0-12h3.851L18.768,7.465z" />
              </g>
            </svg>
          </div>
        </div>
      </a>
    );
  }
}

export default withStyles(s)(FacebookShareButton);
