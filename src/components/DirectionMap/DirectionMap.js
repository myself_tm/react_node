import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import GoogleMapLoader from 'react-google-maps/lib/GoogleMapLoader';
import { GoogleMap, DirectionsRenderer } from 'react-google-maps';
import { NotificationManager } from 'react-notifications';

class DirectionMap extends React.Component {
  static propTypes = {
    start: PropTypes.object,
    destination: PropTypes.object,
    travelMode: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      start: props.start,
      destination: props.destination,
    };
  }

  componentDidMount() {
    if (google) {
      const DirectionsService = new google.maps.DirectionsService();

      let travelMode = google.maps.TravelMode.DRIVING;
      if (this.props.travelMode === 'bike') {
        travelMode = google.maps.TravelMode.BICYCLING;
      } else if (this.props.travelMode === 'walk') {
        travelMode = google.maps.TravelMode.WALKING;
      }

      DirectionsService.route({
        origin: new google.maps.LatLng(this.state.start.lat, this.state.start.lng),
        destination: new google.maps.LatLng(this.state.destination.lat, this.state.destination.lng),
        travelMode: travelMode,     // eslint-disable-line object-shorthand
      }, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            travelMode: travelMode, // eslint-disable-line object-shorthand
            directions: result,
          });
        } else {
          NotificationManager.error('Error fetching directions');
        }
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      start: nextProps.start,
      destination: nextProps.destination,
    });

    if (google) {
      const DirectionsService = new google.maps.DirectionsService();

      let travelMode = google.maps.TravelMode.DRIVING;
      if (nextProps.travelMode === 'bike') {
        travelMode = google.maps.TravelMode.BICYCLING;
      } else if (nextProps.travelMode === 'walk') {
        travelMode = google.maps.TravelMode.WALKING;
      }

      DirectionsService.route({
        origin: new google.maps.LatLng(this.state.start.lat, this.state.start.lng),
        destination: new google.maps.LatLng(this.state.destination.lat, this.state.destination.lng),
        travelMode: travelMode,     // eslint-disable-line object-shorthand
      }, (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            travelMode: travelMode, // eslint-disable-line object-shorthand
            directions: result,
          });
        } else {
          NotificationManager.error(`error fetching directions ${result}`);
        }
      });
    }
  }

  render() {
    return (
      <GoogleMapLoader
        containerElement={
          <div
            style={{
              height: '100%',
            }}
          />
        }
        googleMapElement={
          <GoogleMap
            defaultZoom={15}
            defaultOptions={{ disableDefaultUI: true }}
            center={this.props.start}
          >
            {this.state.directions && <DirectionsRenderer directions={this.state.directions} />}
          </GoogleMap>
        }
      />
    );
  }
}

const mapState = (state) => ({
  travelMode: state.venues.filters.transport.value,
});

const mapDispatch = {
};

const EnhancedContent = connect(mapState, mapDispatch)(DirectionMap);

export default EnhancedContent;
