import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import history from '../../core/history';
import { answerQuestion as answerQuestionAction } from '../../actions/quiz';
import { loadPhone as loadPhoneAction, sendMessage as sendMessageAction } from '../../actions/group';
import { getUserInfo } from '../../reducers/user';
import s from './QuestionItem.css';

class QuestionItem extends React.Component {
  static propTypes = {
    imgUrl: PropTypes.string,
    text: PropTypes.string,
    questionId: PropTypes.string,
    userId: PropTypes.string,
    groupId: PropTypes.string,
    isQuizSingle: PropTypes.bool,
    isAdmin: PropTypes.bool,
    answerQuestion: PropTypes.func,
    loadPhone: PropTypes.func,
    sendMessage: PropTypes.func,
  };

  render() {
    const { questionId, imgUrl, text, userId, groupId, isQuizSingle, isAdmin, answerQuestion, loadPhone, sendMessage, ...props } = this.props;     // eslint-disable-line max-len
    let img = require('../../public/img/1.jpg');    // eslint-disable-line global-require, import/no-dynamic-require
    if (imgUrl) {
      img = require(`../../public/img/${imgUrl}`);  // eslint-disable-line global-require, import/no-dynamic-require
    }

    return (
      <a
        {...props}
        href={`/question/${questionId}`}
        className={`answer-item ${s.answer_item}`}
        onClick={async e => {
          e.preventDefault();
          try {
            const { type, nextQuestionId } = await answerQuestion({
              groupId: groupId,       // eslint-disable-line object-shorthand
              userId: userId,         // eslint-disable-line object-shorthand
              questionId: questionId, // eslint-disable-line object-shorthand
              answer: text,           // eslint-disable-line object-shorthand
            });
            if (type === 'question') {
              history.push(`/question/${nextQuestionId}`);
            } else if (isQuizSingle) {
              history.push('/result/0');
            } else if (type === 'quizCompleted' && !isAdmin) {
              const usersWithPhone = await loadPhone({ groupId });
              for (let index = 0; index < usersWithPhone.length; index += 1) {
                const url = `http://localhost:3001/resultGroup/${groupId}/${usersWithPhone[index][0]}`;
                const msg = `We've got a match! Yazabi's found a great place for you and your friends. Follow this link to see it:%0${url}`;
                await sendMessage({ phoneNumber: usersWithPhone[index][1], msg });
              }
              history.push('/result/0');
            } else {
              history.push('/phone');
            }
          } catch (err) {
            throw (err);
          }
        }}
      >
        <img src={img} alt="option" />
        <span className={'description'}>{text}</span>
      </a>
    );
  }
}

const mapState = (state) => ({
  userId: getUserInfo(state).userId,
  groupId: getUserInfo(state).groupId,
  isQuizSingle: state.quiz.isSingle,
  isAdmin: state.user.isAdmin,
});

const mapDispatch = {
  answerQuestion: answerQuestionAction,
  loadPhone: loadPhoneAction,
  sendMessage: sendMessageAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(QuestionItem);

export default withStyles(s)(EnhancedContent);
