import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { NotificationContainer } from 'react-notifications';
import LoadingBar from 'react-redux-loading-bar';
import s from './Layout.css';

class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  render() {
    return (
      <div>
        <LoadingBar style={{ backgroundColor: 'green', height: '5px', zIndex: 10000 }} />
        <NotificationContainer />
        {this.props.children}
      </div>
    );
  }
}

export default withStyles(s)(Layout);
