import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ResultImageSlider.css';

class NextArrow extends React.Component {
  render() {
    const { ...props } = this.props;
    if (props.className.includes('slick-disabled')) {
      return (
        <div {...props} className={`${s.arrow} ${s.arrow_next} ${s.disabled}`}>
          <i className={`icon material-icon ${s.icon}`}>keyboard_arrow_right</i>
        </div>
      );
    }

    return (
      <div {...props} className={`${s.arrow} ${s.arrow_next}`}>
        <i className={`icon material-icon ${s.icon}`}>keyboard_arrow_right</i>
      </div>
    );
  }
}

export default withStyles(s)(NextArrow);
