import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import CarouselSlider from 'react-slick';
import PrevArrow from './PrevArrow.js';
import NextArrow from './NextArrow.js';
import s from './ResultImageSlider.css';

class ResultImageSlider extends React.Component {
  static propTypes = {
    photoUrls: PropTypes.array,
  };

  constructor() {
    super();
    this.state = {
      sliderSetting: {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        }],
      },
    };
  }

  render() {
    this.state.sliderSetting.prevArrow = <PrevArrow />;
    this.state.sliderSetting.nextArrow = <NextArrow />;
    const sliderImgs = this.props.photoUrls.map((photoUrl, index) => {
      return (
        <div key={index} className={s.image_container}>
          <div className={s.image} style={{ backgroundImage: `url(${photoUrl})` }} />
        </div>
      );
    });

    return (
      <CarouselSlider
        {...this.state.sliderSetting}
      >
        {sliderImgs}
      </CarouselSlider>);
  }
}

export default withStyles(s)(ResultImageSlider);
