import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import GoogleMapLoader from 'react-google-maps/lib/GoogleMapLoader';
import { GoogleMap, Marker } from 'react-google-maps';
import { getVenues as getVenuesAction } from '../../actions/venues';
import { setGroupId as setGroupIdAction } from '../../actions/user';
import { selectSearchFilter, selectVenues } from '../../reducers/venues';
import { getUserLocation } from '../../reducers/user';

const defaultCenter = { lat: 60.192059, lng: 24.945831 };

class VenuesMap extends React.Component {
  static propTypes = {
    venues: PropTypes.object,
    filters: PropTypes.object,
    selectSearchFilter: PropTypes.func,
    selectVenues: PropTypes.func,
    getVenues: PropTypes.func,
    setGroupId: PropTypes.func,
    defaultCenter: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      positions: props.venues.positions || [],
    };
    if (props.filters.location) {
      if (props.filters.location.location) {
        this.state.center = props.filters.location.location;
      }
    } else {
      this.state.center = { lat: props.defaultCenter.lat || defaultCenter.lat, lng: props.defaultCenter.lng || defaultCenter.lng };    // eslint-disable-line max-len
    }
  }

  async componentWillReceiveProps(nextProps) {
    if (nextProps.filters.location && nextProps.filters.transport && nextProps.filters.time) {
      const curFilters = {
        transport: nextProps.filters.transport ? nextProps.filters.transport : { value: null },
        time: nextProps.filters.time ? nextProps.filters.time : { value: null },
      };
      const orgFilters = {
        transport: this.props.filters.transport ? this.props.filters.transport : { value: null },
        time: this.props.filters.time ? this.props.filters.time : { value: null },
      };

      if (nextProps.filters.location) {
        if (nextProps.filters.location.location) {
          orgFilters.location = nextProps.filters.location.location;
        } else {
          orgFilters.location = { lat: null, lng: null };
        }
      }
      if (this.props.filters.location) {
        if (this.props.filters.location.location) {
          curFilters.location = this.props.filters.location.location;
        } else {
          curFilters.location = { lat: null, lng: null };
        }
      }

      if (
        (orgFilters.location.lat !== curFilters.location.lat) ||
        (orgFilters.time.value !== curFilters.time.value) ||
        (orgFilters.transport.value !== curFilters.transport.value)
      ) {
        const response = await this.props.getVenues({
          locationFilter: nextProps.filters.location,
          timeFilter: nextProps.filters.transport,
          transportFilter: nextProps.filters.time,
        });

        nextProps.setGroupId({ value: response.groupId });

        const result = [];
        for (let index = 0; index < response.venuesInArea; index += 1) {
          result.push({
            position: { lat: response.venueLats[index], lng: response.venueLongs[index] },
          });
        }
        this.setState({ positions: result, center: curFilters.location });
      }
    } else if (!nextProps.filters.location) {
      this.setState({
        center: { lat: nextProps.defaultCenter.lat || defaultCenter.lat, lng: nextProps.defaultCenter.lng || defaultCenter.lng },        // eslint-disable-line max-len
      });
    }
  }

  showVenues = (venues) => {
    if (venues && venues.counts > 0 && venues.positions) {
      return venues.positions.map((venue, $index) => (
        <Marker
          key={$index}
          {...venue}
        />
      ));
    }

    return null;
  }

  render() {
    const { venues } = this.props;

    return (
      <GoogleMapLoader
        containerElement={
          <div
            style={{
              height: '100%',
            }}
          />
        }
        googleMapElement={
          <GoogleMap
            defaultZoom={15}
            defaultOptions={{ disableDefaultUI: true }}
            center={this.state.center}
            ref={e => (this.map = e)}
            onTilesloaded={() => {
              if (google && google.maps && (this.state.positions.length > 0)) {
                const { LatLng }  = google.maps;
                const bounds = this.state.positions.map(position => {
                  return LatLng(position.position.lat, position.position.lng);
                });
                this.map.fitBounds(bounds);
              }
            }}
          >
            {
              this.showVenues(venues)
            }
          </GoogleMap>
        }
      />
    );
  }
}

const mapState = (state, props) => ({
  venues: selectVenues(state, props),
  filters: selectSearchFilter(state, props),
  defaultCenter: getUserLocation(state, props),
});

const mapDispatch = {
  getVenues: getVenuesAction,
  setGroupId: setGroupIdAction,
};

export default connect(mapState, mapDispatch)(VenuesMap);
