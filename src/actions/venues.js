/* eslint-disable import/prefer-default-export */

import fetch from '../core/fetch';

import {
  SET_FILTER_LOCATION,
  SET_FILTER_TRANSPORT,
  SET_FILTER_TIME,
  FETCH_VENUES_START,
  FETCH_VENUES_SUCCESS,
  FETCH_VENUES_FAILURE,
} from '../constants/actions';

export function setFilterLocation({ value }) {
  return (dispatch) => {
    dispatch({
      type: SET_FILTER_LOCATION,
      payload: {
        value,
      },
    });

    return true;
  };
}

export function setFilterTransport({ value }) {
  return (dispatch) => {
    dispatch({
      type: SET_FILTER_TRANSPORT,
      payload: {
        value,
      },
    });

    return true;
  };
}

export function setFilterTime({ value }) {
  return (dispatch) => {
    dispatch({
      type: SET_FILTER_TIME,
      payload: {
        value,
      },
    });

    return true;
  };
}

export function getVenues({ locationFilter, timeFilter, transportFilter }) {
  return async (dispatch) => {
    dispatch({
      type: FETCH_VENUES_START,
    });

    try {
      const dist = timeFilter.value * transportFilter.unit * 60;
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          radius: dist,
          location: [locationFilter.location.lat, locationFilter.location.lng],
          type: 'venues',
        }),
      });

      const result = await resp.json();
      
      dispatch({
        type: FETCH_VENUES_SUCCESS,
        payload: {
          result,
        },
      });
      return result;
    } catch (err) {
      dispatch({
        type: FETCH_VENUES_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
  };
}
