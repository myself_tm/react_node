/* eslint-disable import/prefer-default-export */

import {
  SET_USER_ID,
  SET_GROUP_ID,
  SET_USER_ADMIN,
  SET_USER_INVITEE,
  SET_USER_LOCATION,
} from '../constants/actions';

export function setUserId({ value }) {
  return (dispatch) => {
    dispatch({
      type: SET_USER_ID,
      payload: {
        value,
      },
    });
  };
}

export function setGroupId({ value }) {
  return (dispatch) => {
    dispatch({
      type: SET_GROUP_ID,
      payload: {
        value,
      },
    });
  };
}

export function setUserAsAdmin() {
  return (dispatch) => {
    dispatch({
      type: SET_USER_ADMIN,
    });
  };
}

export function setUserAsInvitee() {
  return (dispatch) => {
    dispatch({
      type: SET_USER_INVITEE,
    });
  };
}

export function setUserLocation({ lat, lng }) {
  return (dispatch) => {
    dispatch({
      type: SET_USER_LOCATION,
      payload: {
        lat: lat,       // eslint-disable-line object-shorthand
        lng: lng,       // eslint-disable-line object-shorthand
      },
    });
  };
}
