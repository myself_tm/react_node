/* eslint-disable import/prefer-default-export */

import fetch from '../core/fetch';

import {
  SET_QUIZ_TYPE,
  FETCH_QUIZ_START,
  FETCH_QUIZ_SUCCESS,
  FETCH_QUIZ_FAILURE,
  SET_ANSWER_START,
  SET_ANSWER_SUCCESS,
  SET_ANSWER_FAILURE,
  QUIZ_SUCCESS,
  JOIN_QUIZ_START,
  JOIN_QUIZ_SUCCESS,
  JOIN_QUIZ_FAILURE,
  SET_USER_ID,
} from '../constants/actions';

export function setQuizTypeAsSingle() {
  return async (dispatch) => {
    dispatch({
      type: SET_QUIZ_TYPE,
      payload: {
        value: true,
      },
    });

    return true;
  };
}

export function setQuizTypeAsGroup() {
  return async (dispatch) => {
    dispatch({
      type: SET_QUIZ_TYPE,
      payload: {
        value: false,
      },
    });

    return true;
  };
}

export function startQuiz({ groupId }) {
  return async (dispatch) => {
    dispatch({
      type: FETCH_QUIZ_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId, // eslint-disable-line object-shorthand
          type: 'start',
        }),
      });
      const result = await resp.json();
      dispatch({
        type: SET_USER_ID,
        payload: {
          value: result.userId,
        },
      });
      dispatch({
        type: FETCH_QUIZ_SUCCESS,
        payload: {
          result,
        },
      });
      return result.questionId;
    } catch (err) {
      dispatch({
        type: FETCH_QUIZ_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
  };
}

export function joinQuiz({ groupId, userId }) {
  return async (dispatch) => {
    dispatch({
      type: JOIN_QUIZ_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,   // eslint-disable-line object-shorthand
          id: userId,         // eslint-disable-line object-shorthand
          type: 'add',
        }),
      });

      const result = await resp.json();
      dispatch({
        type: JOIN_QUIZ_SUCCESS,
        payload: {
          result,
        },
      });
      return result.questionId;
    } catch (err) {
      dispatch({
        type: JOIN_QUIZ_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
  };
}

export function answerQuestion({ groupId, userId, questionId, answer }) {
  return async (dispatch) => {
    dispatch({
      type: SET_ANSWER_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,       // eslint-disable-line object-shorthand
          id: userId,
          questionId: questionId, // eslint-disable-line object-shorthand
          answer: answer,         // eslint-disable-line object-shorthand
          type: 'question',
        }),
      });

      const result = await resp.json();
      if (result.type === 'question') {
        dispatch({
          type: SET_ANSWER_SUCCESS,
          payload: {
            result,
            answer,
          },
        });
      } else {
        dispatch({
          type: QUIZ_SUCCESS,
          payload: {
            answer,
          },
        });
      }
      return { type: result.type, nextQuestionId: result.questionId, idList: result.idList };
    } catch (err) {
      dispatch({
        type: SET_ANSWER_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
  };
}
