/* eslint-disable import/prefer-default-export */

import fetch from '../core/fetch';

import {
  FETCH_RESULT_START,
  FETCH_RESULT_SUCCESS,
  FETCH_RESULT_FAILURE,
} from '../constants/actions';

export function getResult({ groupId, userId, rank }) {
  return async (dispatch) => {
    dispatch({
      type: FETCH_RESULT_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,       // eslint-disable-line object-shorthand
          id: userId,             // eslint-disable-line object-shorthand
          venueRank: parseInt(rank, 10),
          type: 'result',
        }),
      });

      const result = await resp.json();
      dispatch({
        type: FETCH_RESULT_SUCCESS,
        payload: {
          result,
          rank,
        },
      });
    } catch (err) {
      dispatch({
        type: FETCH_RESULT_FAILURE,
        payload: {
          rank,
          err,
        },
      });
      return false;
    }

    return true;
  };
}
