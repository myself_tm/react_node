/* eslint-disable import/prefer-default-export */

import {
  GROUP_ADD_MEMBER,
  GROUP_UPDATE_MEMBER,
  SET_PHONES_START,
  SET_PHONES_SUCCESS,
  SET_PHONES_FAILURE,
  FETCH_PHONES_START,
  FETCH_PHONES_SUCCESS,
  FETCH_PHONES_FAILURE,
  SEND_SMS_START,
  SEND_SMS_SUCCESS,
  SEND_SMS_FAILURE,
} from '../constants/actions';

export function addMember({ phoneNo }) {
  return {
    type: GROUP_ADD_MEMBER,
    payload: {
      phoneNo,
    },
  };
}

export function updateMember({ userInd, phoneNo }) {
  return {
    type: GROUP_UPDATE_MEMBER,
    payload: {
      userInd,
      phoneNo,
    },
  };
}

export function sendMessage({ phoneNumber, msg }) {
  return async (dispatch) => {
    dispatch({
      type: SEND_SMS_START,
    });

    try {
      const resp = await fetch('/twilio', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          phoneNumber: phoneNumber,   // eslint-disable-line object-shorthand
          message: msg,
        }),
      });
      await resp.json();
      dispatch({
        type: SEND_SMS_SUCCESS,
      });
    } catch (err) {
      dispatch({
        type: SEND_SMS_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
    return true;
  };
}

export function savePhone({ groupId, phoneNumbers }) {
  return async (dispatch) => {
    dispatch({
      type: SET_PHONES_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,             // eslint-disable-line object-shorthand
          phone: phoneNumbers,   // eslint-disable-line object-shorthand
          type: 'savePhone',
        }),
      });
      const result = await resp.json();
      dispatch({
        type: SET_PHONES_SUCCESS,
      });
      return { usersWithPhone: result.phone };
    } catch (err) {
      dispatch({
        type: SET_PHONES_FAILURE,
      });
      return false;
    }
  };
}

export function saveAdminPhoneNumber({ groupId, userId, phoneNumbers }) {
  return async (dispatch) => {
    dispatch({
      type: SET_PHONES_START,
    });

    try {
      await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,             // eslint-disable-line object-shorthand
          id: userId,               // eslint-disable-line object-shorthand
          phone: phoneNumbers,          // eslint-disable-line object-shorthand
          type: 'savePhone',
        }),
      });
      dispatch({
        type: SET_PHONES_SUCCESS,
      });
      return true;
    } catch (err) {
      dispatch({
        type: SET_PHONES_FAILURE,
      });
      return false;
    }
  };
}

export function loadPhone({ groupId }) {
  return async (dispatch) => {
    dispatch({
      type: FETCH_PHONES_START,
    });

    try {
      const resp = await fetch('/python', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          groupId: groupId,             // eslint-disable-line object-shorthand
          type: 'loadPhone',
        }),
      });
      const data = await resp.json();
      dispatch({
        type: FETCH_PHONES_SUCCESS,
        payload: {
          usersWithPhone: data.phone,   // eslint-disable-line object-shorthand
        },
      });
      return { usersWithPhone: data.phone };
    } catch (err) {
      dispatch({
        type: FETCH_PHONES_FAILURE,
        payload: {
          err,
        },
      });
      return false;
    }
  };
}
