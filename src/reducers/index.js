import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import user from './user';
import runtime from './runtime';
import intl from './intl';
import content from './content';
import group from './group';
import venues from './venues';
import quiz from './quiz';
import result from './result';

export default combineReducers({
  user,
  runtime,
  intl,
  content,
  group,
  venues,
  result,
  quiz,
  loadingBar: loadingBarReducer,
});
