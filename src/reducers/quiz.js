import {
  SET_QUIZ_TYPE,
  FETCH_QUIZ_START,
  FETCH_QUIZ_SUCCESS,
  FETCH_QUIZ_FAILURE,
  JOIN_QUIZ_START,
  JOIN_QUIZ_SUCCESS,
  JOIN_QUIZ_FAILURE,
  SET_ANSWER_START,
  SET_ANSWER_SUCCESS,
  SET_ANSWER_FAILURE,
  QUIZ_SUCCESS,
} from '../constants/actions';

// selectors
export function isQuizStarted(state) {
  if (state.quiz && state.quiz.isStarted) {
    return true;
  }
  return false;
}

export function selectQuestion(state, questionId) {
  if (state.quiz && state.quiz.questions) {
    for (let index = state.quiz.questions.length - 1; index >= 0; index -= 1) {
      if (state.quiz.questions[index].questionId === questionId) {
        return state.quiz.questions[index];
      }
    }
  }
  return {};
}

// reducer
export default function quiz(state = {}, action) {
  switch (action.type) {
    case SET_QUIZ_TYPE: {
      return {
        ...state,
        isSingle: action.payload.value,
      };
    }

    case FETCH_QUIZ_START: {
      return {
        ...state,
        isFetching: true,
        isStarted: false,
      };
    }

    case FETCH_QUIZ_SUCCESS: {
      return {
        ...state,
        questions: [action.payload.result],
        answers: [],
        isFetching: false,
        isStarted: true,
      };
    }

    case FETCH_QUIZ_FAILURE: {
      return {
        ...state,
        isFetching: false,
        errMsg: action.payload.err,
        isStarted: false,
      };
    }

    case JOIN_QUIZ_START: {
      return {
        ...state,
        isFetching: true,
        isStarted: false,
      };
    }

    case JOIN_QUIZ_SUCCESS: {
      return {
        ...state,
        questions: [action.payload.result],
        answers: [],
        isFetching: false,
        isStarted: true,
      };
    }

    case JOIN_QUIZ_FAILURE: {
      return {
        ...state,
        isFetching: false,
        errMsg: action.payload.err,
        isStarted: false,
      };
    }

    case SET_ANSWER_START: {
      return {
        ...state,
        isFetching: true,
      };
    }

    case SET_ANSWER_SUCCESS: {
      return {
        ...state,
        questions: [
          ...state.questions,
          action.payload.result,
        ],
        answers: [
          ...state.answers,
          action.payload.answer,
        ],
        isFetching: false,
      };
    }

    case SET_ANSWER_FAILURE: {
      return {
        ...state,
        isFetching: false,
        errMsg: action.payload.err,
      };
    }

    case QUIZ_SUCCESS: {
      return {
        ...state,
        answers: [
          ...state.answers,
          action.payload.answer,
        ],
        isFetching: false,
      };
    }
    default:
      return state;
  }
}
