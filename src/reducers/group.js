import {
  GROUP_ADD_MEMBER,
  GROUP_UPDATE_MEMBER,
  FETCH_PHONES_START,
  FETCH_PHONES_SUCCESS,
  FETCH_PHONES_FAILURE,
} from '../constants/actions';

// selectors
export function selectUsers(state = {}) {
  if (state.group && state.group.users) {
    return state.group.users;
  }
  return [];
}

// reducer
export default function group(state = {}, action) {
  switch (action.type) {
    case GROUP_ADD_MEMBER: {
      state.users = state.users ? state.users : [];     // eslint-disable-line no-param-reassign
      return {
        ...state,
        users: [
          ...state.users,
          action.payload.phoneNo,
        ],
      };
    }

    case GROUP_UPDATE_MEMBER: {
      return {
        ...state,
        users: [
          ...state.users.slice(0, action.payload.userInd),
          action.payload.phoneNo,
          ...state.users.slice(action.payload.userInd + 1),
        ],
      };
    }

    case FETCH_PHONES_START: {
      return {
        ...state,
        usersWithPhone: [],
        isFetching: true,
      };
    }

    case FETCH_PHONES_SUCCESS: {
      return {
        ...state,
        usersWithPhone: action.payload.usersWithPhone,
        isFetching: false,
      };
    }

    case FETCH_PHONES_FAILURE: {
      return {
        ...state,
        errMsg: action.payload.err,
      };
    }

    default: {
      return state;
    }
  }
}
