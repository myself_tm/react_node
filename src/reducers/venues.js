import {
  SET_FILTER_LOCATION,
  SET_FILTER_TRANSPORT,
  SET_FILTER_TIME,
  FETCH_VENUES_START,
  FETCH_VENUES_SUCCESS,
  FETCH_VENUES_FAILURE,
} from '../constants/actions';

const defaultState = {
  filters: {},
};

// selectors
export function selectSearchFilter(state) {
  if (state.venues && state.venues.filters) {
    return {
      location: state.venues.filters.location || null,
      transport: state.venues.filters.transport || null,
      time: state.venues.filters.time || null,
    };
  }

  return {
    location: null,
    transport: null,
    time: null,
  };
}

export function selectVenues(state) {
  if (state.venues) {
    return {
      positions: state.venues.positions || [],
      counts: state.venues.counts || 0,
      isFetching: state.venues.isFetching,
    };
  }

  return {
    positions: [],
    counts: 0,
    isFetching: false,
  };
}

// reducer
export default function venues(state = defaultState, action) {
  switch (action.type) {
    case SET_FILTER_LOCATION: {
      return {
        ...state,
        filters: {
          ...state.filters,
          location: action.payload.value,
        },
        positions: [],
        isFetching: false,
        counts: 0,
      };
    }

    case SET_FILTER_TRANSPORT: {
      return {
        ...state,
        filters: {
          ...state.filters,
          transport: action.payload.value,
        },
        positions: [],
        isFetching: false,
        counts: 0,
      };
    }

    case SET_FILTER_TIME: {
      return {
        ...state,
        filters: {
          ...state.filters,
          time: action.payload.value,
        },
        positions: [],
        isFetching: false,
        counts: 0,
      };
    }

    case FETCH_VENUES_START: {
      return {
        ...state,
        positions: [],
        counts: 0,
        isFetching: true,
      };
    }

    case FETCH_VENUES_SUCCESS: {
      const data = action.payload.result;
      const result = [];
      for (let index = 0; index < data.venuesInArea; index += 1) {
        result.push({
          position: { lat: data.venueLats[index], lng: data.venueLongs[index] },
        });
      }

      return {
        ...state,
        positions: result,
        counts: action.payload.result.venuesInArea,
        isFetching: false,
      };
    }

    case FETCH_VENUES_FAILURE: {
      return {
        ...state,
        isFetching: false,
        errMsg: action.payload.err,
      };
    }

    default: {
      return state;
    }
  }
}
