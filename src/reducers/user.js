import {
  SET_USER_ID,
  SET_GROUP_ID,
  SET_USER_ADMIN,
  SET_USER_INVITEE,
  SET_USER_LOCATION,
} from '../constants/actions';

// selectors
export function getUserInfo(state) {
  if (state.user) {
    return { userId: state.user.userId, groupId: state.user.groupId };
  }
  return { userId: null, groupId: null };
}

export function getUserLocation(state) {
  if (state.user && state.user.location) {
    return { lat: state.user.location.lat, lng: state.user.location.lng };
  }
  return { lat: null, lng: null };
}

// reducer
export default function user(state = {}, action) {
  switch (action.type) {
    case SET_USER_ID: {
      return {
        ...state,
        userId: action.payload.value,
      };
    }

    case SET_GROUP_ID: {
      return {
        ...state,
        groupId: action.payload.value,
      };
    }

    case SET_USER_ADMIN: {
      return {
        ...state,
        isAdmin: true,
      };
    }

    case SET_USER_INVITEE: {
      return {
        ...state,
        isAdmin: true,
      };
    }

    case SET_USER_LOCATION: {
      return {
        ...state,
        location: {
          lat: action.payload.lat,
          lng: action.payload.lng,
        },
      };
    }

    default:
      return state;
  }
}
