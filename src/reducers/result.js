import {
  FETCH_RESULT_START,
  FETCH_RESULT_SUCCESS,
  FETCH_RESULT_FAILURE,
} from '../constants/actions';

// selectors
export function isLastResult(state, rank) {
  if (state.venues && (rank === (state.venues.counts - 1))) {
    return true;
  }
  return false;
}
export function selectResult(state, rank) {
  if (state.result && state.result.results && state.result.results.length > 0) {
    return state.result.results[rank];
  }
  return {
    photoUrls: [],
    infoGraphic: [],
    matchingReport: [],
  };
}

export default function result(state = {}, action) {
  switch (action.type) {
    case FETCH_RESULT_START: {
      return {
        ...state,
        isFetching: true,
      };
    }
    case FETCH_RESULT_SUCCESS: {
      const rank = parseInt(action.payload.rank, 10);
      if (rank !== 0) {
        return {
          ...state,
          results: [
            ...state.results.slice(0, rank),
            action.payload.result,
            ...state.results.slice(rank + 1),
          ],
          isFetching: false,
        };
      }
      return {
        ...state,
        results: [
          action.payload.result,
        ],
        isFetching: false,
      };
    }
    case FETCH_RESULT_FAILURE: {
      return {
        ...state,
        isFetching: false,
      };
    }
    default:
      return state;
  }
}
