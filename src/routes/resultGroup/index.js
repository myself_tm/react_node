
import React from 'react';
import Layout from '../../components/Layout';
import Result from '../result/Result';
import { setGroupId, setUserId, setUserAsInvitee } from '../../actions/user';

export default {

  path: ['/resultGroup/:groupId/:userId', '/join'],

  async action({ store, params }) {
    const { groupId, userId } = params;
    if (groupId && userId) {
      store.dispatch(setGroupId({ value: groupId }));
      store.dispatch(setUserId({ value: userId }));
      setUserAsInvitee();
      return {
        component: <Layout><Result rank={0} /></Layout>,
      };
    }
    return { redirect: '/' };
  },

};
