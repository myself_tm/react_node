import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import history from '../../core/history';
import Link from '../../components/Link';
import { saveAdminPhoneNumber as saveAdminPhoneNumberAction } from '../../actions/group';
import s from '../invite/Invite.css';

class Phone extends React.Component {
  static propTypes = {
    isMobile: PropTypes.bool,
    isAdmin: PropTypes.bool,
    groupId: PropTypes.string,
    userId: PropTypes.string,
    saveAdminPhoneNumber: PropTypes.func,
  };

  desktopBody = () => {
    if (this.props.isAdmin) {
      return (
        <div>
          <input
            id="address"
            type="tel"
            name="address"
            ref={e => (this.phone = e)}
            className={`input-search text-center ${s.input_search}`}
            placeholder="phone number"
            autoFocus
          />
          <a
            href="/complete"
            className={`btn btn-primary ${s.btn}`}
            onClick={async e => {
              e.preventDefault();
              this.props.saveAdminPhoneNumber({
                groupId: this.props.groupId,
                userId: this.props.userId,
                phoneNumbers: [this.phone.value],
              });
              history.push('/complete');
            }}
          >
            Submit
          </a>
        </div>
      );
    }
    return <Link className={`btn btn-primary ${s.btn}`} to="/type">Start Again</Link>;
  }

  mobileBody = () => {
    if (this.props.isAdmin) {
      return (
        <input
          id="address"
          type="tel"
          name="address"
          ref={e => (this.phone = e)}
          className={`input-search text-center ${s.input_search}`}
          placeholder="phone number"
          autoFocus
        />
      );
    }
    return false;
  }

  mobileFooter = () => {
    if (this.props.isAdmin) {
      return (
        <a
          href="/complete"
          className={`btn btn-primary ${s.btn}`}
          onClick={async e => {
            e.preventDefault();
            this.props.saveAdminPhoneNumber({
              groupId: this.props.groupId,
              userId: this.props.userId,
              phoneNumbers: [this.phone.value],
            });
            history.push('/complete');
          }}
        >
            Submit
        </a>
      );
    }
    return <Link className={`btn btn-primary ${s.btn}`} to="/type">Start Again</Link>;
  }

  renderDesktop = () => {
    return (
      <div className={`${s.root} ${s.complete}`}>
        <section className={'header has-background background-normal'}>
          <h1>Enter your phone number and we'll send you the match!</h1>
        </section>
        <section className={`main full has-background background-orange text-center ${s.main} ${s.full} ${s.has_no_footer}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            <p>We'll send you the group's match when everyone finishes the quiz.</p>
            { this.desktopBody() }
          </div>
        </section>
      </div>
    );
  };

  renderMobile = () => {
    return (
      <div className={`${s.root} ${s.mobile} ${s.complete}`}>
        <section className={'header has-background background-normal'}>
          <h1>Enter your phone number and we'll send you the match!</h1>
        </section>
        <section className={`main full has-background background-orange text-center ${s.main} ${s.full}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            <p>We'll send you the group's match when everyone finishes the quiz.</p>
            { this.mobileBody() }
          </div>
        </section>
        <section className={'footer fixed has-background background-image text-center'}>
          { this.mobileFooter() }
        </section>
      </div>
    );
  };

  render() {
    const { isMobile } = this.props;
    if (isMobile) {
      return this.renderMobile();
    }
    return this.renderDesktop();
  }

}

const mapState = (state) => ({
  isMobile: state.runtime.isMobile,
  isAdmin: state.user.isAdmin,
  groupId: state.user.groupId,
  userId: state.user.userId,
});

const mapDispatch = {
  saveAdminPhoneNumber: saveAdminPhoneNumberAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Phone);

export default withStyles(s)(EnhancedContent);
