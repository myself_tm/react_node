import React from 'react';
import Layout from '../../components/Layout';
import Phone from './Phone';

export default {

  path: '/phone',

  action() {
    return {
      component: <Layout><Phone /></Layout>,
    };
  },

};
