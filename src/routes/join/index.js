import React from 'react';
import Layout from '../../components/Layout';
import Question from '../question/Question';
import { setGroupId, setUserId, setUserAsInvitee } from '../../actions/user';
import { setFilterLocation, setFilterTransport, setFilterTime, getVenues } from '../../actions/venues';
import { joinQuiz } from '../../actions/quiz';

export default {

  path: ['/join/:groupId/:userId/:filters', '/join'],

  async action({ store, params }) {
    const { groupId, userId, filters } = params;
    let questionId = null;
    if (groupId && userId && filters) {
      store.dispatch(setGroupId({ value: groupId }));
      store.dispatch(setUserId({ value: userId }));

      const filtersObject = JSON.parse(filters);
      store.dispatch(setFilterLocation({
        value: {
          location: filtersObject.location,
        },
      }));
      store.dispatch(setFilterTransport({
        value: {
          unit: filtersObject.transport,
        },
      }));
      store.dispatch(setFilterTime({
        value: {
          value: filtersObject.time,
        },
      }));

      try {
        const venues = await store.dispatch(getVenues({
          locationFilter: {
            location: filtersObject.location,
          },
          timeFilter: filtersObject.time,
          transportFilter: filtersObject.transport,
        }));
        questionId = await store.dispatch(joinQuiz({ groupId: groupId, userId: userId }));  // eslint-disable-line object-shorthand, max-len
        setUserAsInvitee();
        return {
          component: <Layout><Question questionId={questionId} /></Layout>,
        };
      } catch (err) {
        throw (err);
      }
    }
    return { redirect: '/' };
  },

};
