import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from '../../components/Link';
import s from './Login.css';

class Login extends React.Component {

  render() {
    return (
      <section className={`main full has-background background-image ${s.main}`}>
        <div className={'col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 form-wrapper'}>
          <h1 className={'allow_smallcap'}>login</h1>
          <form className={'col-xs-12'}>
            <input
              id="username"
              type="text"
              name="username"
              placeholder="Email"
              className={'input-transparent full-width'}
              autoFocus
            />
            <input
              id="password"
              type="password"
              name="password"
              placeholder="Password"
              className={'input-transparent full-width'}
            />
            <Link className={'btn center-block btn-transparent full-width'} to="/login">
              Log in
            </Link>
            <Link className={'btn center-block btn-transparent full-width'} to="/signup">
              SignUp
            </Link>
            <Link className={'btn center-block btn-transparent full-width'} to="/type">
              Continue as guest
            </Link>
          </form>
        </div>
      </section>
    );
  }

}

export default withStyles(s)(Login);
