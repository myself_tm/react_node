import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Link from '../../components/Link';
import s from '../invite/Invite.css';

class Sent extends React.Component {
  static propTypes = {
    questionId: PropTypes.number,
  };

  render() {
    return (
      <div className={s.root}>
        <section className={'header has-background background-normal'}>
          <h1>Add your friends:</h1>
          <h1><strong>Decide as a group</strong></h1>
        </section>
        <section className={`main full has-background background-orange text-center ${s.main} ${s.full}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            <h1><strong>Invitations sent</strong><i className={`icon material-icon ${s.icon}`}>tag_faces</i></h1>
            <Link className={`btn btn-primary ${s.btn}`} to="/invite">
              add more
              <i className={`icon material-icon ${s.icon}`}>person_add</i>
            </Link>
          </div>
        </section>
        <section className={`footer fixed has-background background-image text-center ${s.footer}`}>
          <Link className={`btn btn-primary ${s.btn}`} to={`/question/${this.props.questionId}`}>
            Back to quiz
          </Link>
        </section>
      </div>
    );
  }

}

const mapState = (state) => ({
  questionId: state.quiz.questions[state.quiz.questions.length - 1].questionId,
});

const mapDispatch = {
};

const EnhancedContent = connect(mapState, mapDispatch)(Sent);

export default withStyles(s)(EnhancedContent);
