import React from 'react';
import Layout from '../../components/Layout';
import Sent from './Sent';

export default {

  path: '/sent',

  action() {
    return {
      component: <Layout><Sent /></Layout>,
    };
  },

};
