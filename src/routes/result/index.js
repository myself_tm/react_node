import React from 'react';
import { NotificationManager } from 'react-notifications';
import Layout from '../../components/Layout';
import Result from './Result';
import { getResult } from '../../actions/result';
import { isLastResult } from '../../reducers/result';
import { getUserInfo } from '../../reducers/user';

export default {

  path: ['/result/:rank', '/result'],

  async action({ store, params }) {
    const state = store.getState();
    let rank = 0;
    if (params.rank) {
      rank = params.rank;
    }

    const isLast = isLastResult(state, rank);
    if (!isLast) {
      const user = getUserInfo(state);
      if (user.groupId && user.userId) {
        await store.dispatch(getResult({ groupId: user.groupId, userId: user.userId, rank }));
        return {
          component: <Layout><Result rank={parseInt(rank, 10)} isLast={isLast} /></Layout>,
        };
      }
      NotificationManager.error('We can find your answers');
      return { redirect: '/type' };
    }
    NotificationManager.error('There is no more result!');
    return { redirect: `/result/${rank - 1}` };
  },

};
