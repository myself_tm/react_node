import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { NotificationManager } from 'react-notifications';
import history from '../../core/history';
import GoogleShareButton from '../../components/GoogleShareButton';
import FacebookShareButton from '../../components/FacebookShareButton';
import TwitterShareButton from '../../components/TwitterShareButton';
import MailShareButton from '../../components/MailShareButton';
import ResultImageSlider from '../../components/ResultImageSlider';
import DirectionMap from '../../components/DirectionMap';
import Link from '../../components/Link';
import { getResult as getResultAction } from '../../actions/result';
import { selectSearchFilter } from '../../reducers/venues';
import { selectResult } from '../../reducers/result';
import { getUserLocation } from '../../reducers/user';
import s from './Result.css';

const defaultCenter = { lat: 60.192059, lng: 24.945831 };

class Result extends React.Component {
  static propTypes = {
    isMobile: PropTypes.bool,
    defaultCenter: PropTypes.object,
    start: PropTypes.object,
    rank: PropTypes.number,
    isLast: PropTypes.bool,
    result: PropTypes.object,
    getResult: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      result: props.result,
      matchQuality: props.result.matchingReport[0],
    };

    this.state.start = props.start.location;
    this.state.matchReport = props.result.matchingReport.reduce((prev, current, index) => {
      if (typeof current === 'string') {
        return prev;
      }
      prev.push(
        (<div className={s.report_item} key={index}>
          <span className={s.icon} dangerouslySetInnerHTML={{ __html: current.icon }} />
          <span className={s.string}>{current.reportString}</span>
        </div>),
      );
      return prev;
    }, []);

    this.state.infoGrahic = props.result.infoGraphic.map((info, index) => {
      if ((index % 2) === 0) {
        return (
          <div className={s.info_item} key={index}>
            <span className={s.icon} dangerouslySetInnerHTML={{ __html: info.icon }} />
            <span className={s.string}>{info.reportString}</span>
          </div>
        );
      }

      return (
        <div className={s.info_item} key={index}>
          <span className={s.string}>{info.reportString}</span>
          <span className={s.icon} dangerouslySetInnerHTML={{ __html: info.icon }} />
        </div>
      );
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.rank !== this.props.rank) {
      const data = {
        result: nextProps.result,
        matchQuality: nextProps.result.matchingReport[0],
      };

      this.state.start = nextProps.start.location;

      data.matchReport = nextProps.result.matchingReport.reduce((prev, current, index) => {
        if (typeof current === 'string') {
          return prev;
        }
        prev.push(
          (<div className={s.report_item} key={index}>
            <span className={s.icon} dangerouslySetInnerHTML={{ __html: current.icon }} />
            <span className={s.string}>{current.reportString}</span>
          </div>),
        );
        return prev;
      }, []);

      data.infoGrahic = nextProps.result.infoGraphic.map((info, index) => {
        if ((index % 2) === 0) {
          return (
            <div className={s.info_item} key={index}>
              <span className={s.icon} dangerouslySetInnerHTML={{ __html: info.icon }} />
              <span className={s.string}>{info.reportString}</span>
            </div>
          );
        }
        return (
          <div className={s.info_item} key={index}>
            <span className={s.string}>{info.reportString}</span>
            <span className={s.icon} dangerouslySetInnerHTML={{ __html: info.icon }} />
          </div>
        );
      });

      this.setState(data);
    }
  }

  renderDesktop = () => {      // eslint-disable-line arrow-body-style, max-len
    return (
      <div className={s.root}>
        <section className={'header fixed has-background background-thin'}>
          <h3>Your top choice is <strong>{this.state.result.name}</strong></h3>
        </section>
        <section className={`main has-background background-orange ${s.main}`}>
          <div className={s.social}>
            <FacebookShareButton appId="145634995501895" url="https://google.com" redirect_uri="/" />
            <MailShareButton />
            <GoogleShareButton />
            <TwitterShareButton />
          </div>
          <div className={s.details}>
            <div className={s.images}>
              <div className={s.slider}>
                <ResultImageSlider photoUrls={this.state.result.photoUrls} />
              </div>
              <p className={'text-center'}>
                Photos courtsey of
                <a href={this.state.result.fsUrl}>
                  <img
                    className={s.foursquare_logo}
                    src={require('../../public/foursquare-logo.png')}   // eslint-disable-line global-require
                    alt="foursquare"
                  />
                </a>
              </p>
            </div>
            <div className={s.address}>
              <p><i className={'material-icon'}>place</i>{this.state.result.address}</p>
              <p><i className={'material-icon'}>local_phone</i>{this.state.result.phone}</p>
            </div>
            <div className={s.quality}>
              <h2><strong>Match quality:{this.state.matchQuality}</strong></h2>
            </div>
            <div className={s.report}>
              {this.state.matchReport}
            </div>
            <div className={`text-center ${s.another}`}>
              <p>Still not sure?</p>
              <a
                href={`/result/${this.props.rank + 1}`}
                className={`btn btn-primary ${s.btn}`}
                onClick={async e => {
                  e.preventDefault();
                  if (!this.props.isLast) {
                    history.push(`/result/${this.props.rank + 1}`);
                    return true;
                  }
                  NotificationManager.error('This is the last result!');
                  return false;
                }}
              >
                Show me another match
              </a>
            </div>
            <div className={s.info_title}>
              <h2>Guess what?</h2>
            </div>
            <div className={s.info}>
              {this.state.infoGrahic}
            </div>
          </div>
          <div className={s.map_container}>
            <DirectionMap start={this.state.start} destination={defaultCenter} />
          </div>
        </section>
      </div>
    );
  }

  renderMobile = () => {        // eslint-disable-line arrow-body-style
    return (
      <div className={`${s.mobile} ${s.root}`}>
        <section className={'header fixed has-background background-thin'}>
          <h1>Your top choice is <strong>{this.state.result.name}</strong></h1>
        </section>
        <section className={`main has-background background-orange ${s.main}`}>
          <div className={s.social}>
            <FacebookShareButton appId="145634995501895" url="https://google.com" redirect_uri="/" />
            <MailShareButton />
            <GoogleShareButton />
            <TwitterShareButton />
          </div>
          <div className={s.images}>
            <div className={s.slider}>
              <ResultImageSlider photoUrls={this.state.result.photoUrls} />
            </div>
            <p className={'text-center'}>
              Photos courtsey of
              <a href={this.state.result.fsUrl}>
                <img
                  className={s.foursquare_logo}
                  src={require('../../public/foursquare-logo.png')}     // eslint-disable-line global-require
                  alt="foursquare"
                />
              </a>
            </p>
          </div>
          <div className={s.address}>
            <p><i className={'material-icon'}>place</i>{this.state.result.address}</p>
            <p><i className={'material-icon'}>local_phone</i>{this.state.result.phone}</p>
          </div>
          <div className={s.quality}>
            <h2><strong>Match quality:{this.state.matchQuality}</strong></h2>
          </div>
          <div className={s.report}>
            {this.state.matchReport}
          </div>
          <div className={`text-center ${s.another}`}>
            <p>Still not sure?</p>
            <a
              href={`/result/${this.props.rank + 1}`}
              className={`btn btn-primary ${s.btn}`}
              onClick={async e => {
                e.preventDefault();
                if (!this.props.isLast) {
                  history.push(`/result/${this.props.rank + 1}`);
                  return true;
                }
                NotificationManager.error('This is the last result!');
                return false;
              }}
            >
              Show me another match
            </a>
          </div>
          <div className={s.info_title}>
            <h2>Guess what?</h2>
          </div>
          <div className={s.info}>
            {this.state.infoGrahic}
          </div>
        </section>
        <section className={`footer fixed has-background background-image text-center ${s.footer}`}>
          <Link className={`btn btn-primary ${s.btn}`} to={`/direction/${JSON.stringify(this.state.defaultCenter)}`}>
            Get directions
          </Link>
        </section>
      </div>
    );
  }

  render() {
    if (this.props.isMobile) {
      return this.renderMobile();
    }
    return this.renderDesktop();
  }
}

const mapState = (state, props) => ({
  isMobile: state.runtime.isMobile,
  start: selectSearchFilter(state).location,
  result: selectResult(state, props.rank),
  defaultCenter: getUserLocation(state, props),
});

const mapDispatch = {
  getResult: getResultAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Result);

export default withStyles(s)(EnhancedContent);
