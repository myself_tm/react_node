import React from 'react';
import Layout from '../../components/Layout';
import Type from './Type';

export default {

  path: '/type',

  action() {
    return {
      component: <Layout><Type /></Layout>,
    };
  },

};
