import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import {
  setQuizTypeAsSingle as setQuizTypeAsSingleAction,
  setQuizTypeAsGroup as setQuizTypeAsGroupAction,
} from '../../actions/quiz';
import s from '../login/Login.css';

class Type extends React.Component {

  static propTypes = {
    setQuizTypeAsSingle: PropTypes.func,
    setQuizTypeAsGroup: PropTypes.func,
  };

  render() {
    const { setQuizTypeAsSingle, setQuizTypeAsGroup } = this.props;

    return (
      <section className={`main full has-background background-image ${s.main}`}>
        <div className={'col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 form-wrapper'}>
          <h2 className={'allow_smallcap'}>how many people will be taking the quiz?</h2>
          <Link
            className={`col-xs-6 text-center ${s.btn}`}
            to={'/location'}
            onClick={() => {
              setQuizTypeAsSingle();
            }}
          >
            <h2 className={'allow_smallcap'}>Single</h2>
            <i className={`material-icon stroke ${s.icon}`} aria-hidden="true">person</i>
          </Link>
          <Link
            className={`col-xs-6 text-center ${s.btn}`}
            to={'/location'}
            onClick={() => {
              setQuizTypeAsGroup();
            }}
          >
            <h2 className={'allow_smallcap'}>Group</h2>
            <i className={`material-icon stroke ${s.icon}`} aria-hidden="true">group</i>
          </Link>
        </div>
      </section>
    );
  }
}

const mapState = (state) => ({
  availableLocale: state.runtime.availableLocales,
  currentLocale: state.intl.locale,
});

const mapDispatch = {
  setQuizTypeAsSingle: setQuizTypeAsSingleAction,
  setQuizTypeAsGroup: setQuizTypeAsGroupAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Type);

export default withStyles(s)(EnhancedContent);
