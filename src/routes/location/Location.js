import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Geosuggest from 'react-geosuggest';
import { NotificationManager } from 'react-notifications';
import TimeDropdown from '../../components/TimeDropdown';
import TransportDropdown from '../../components/TransportDropdown';
import VenuesMap from '../../components/VenuesMap';
import history from '../../core/history';
import { setFilterLocation as setFilterLocationAction, getVenues as getVenuesAction } from '../../actions/venues';
import { startQuiz as startQuizAction } from '../../actions/quiz';
import { setUserAsAdmin as setUserAsAdminAction, setGroupId as setGroupIdAction } from '../../actions/user';
import { selectSearchFilter, selectVenues } from '../../reducers/venues';
import { getUserInfo } from '../../reducers/user';
import s from './Location.css';
import logo from '../../public/logo.png';

class Location extends React.Component {
  static propTypes = {
    isMobile: PropTypes.bool,
    venues: PropTypes.object,
    filters: PropTypes.object,
    groupId: PropTypes.string,
    selectSearchFilter: PropTypes.func,
    selectVenues: PropTypes.func,
    setFilterLocation: PropTypes.func,
    getVenues: PropTypes.func,
    setGroupId: PropTypes.func,
    startQuiz: PropTypes.func,
    setUserAsAdmin: PropTypes.func,
    setUserAsInvitee: PropTypes.func,
  }

  renderDesktop() {
    const { filters, setFilterLocation, getVenues, setGroupId, startQuiz, setUserAsAdmin } = this.props;
    return (
      <div className={s.root}>
        <section className={'header fixed'}>
          <h2>Make a delicious decision.</h2>
        </section>
        <section className={`main full has-background background-image ${s.main}`}>
          <div className={'container'}>
            <h1>Find the right restaurant for me:</h1>
            <div className={s.search_section}>
              <div className={`input-search-wrapper ${s.input_wrapper}`}>
                <i className={'search_icon material-icon'}>search</i>
                <Geosuggest
                  id="address"
                  name="address"
                  value={filters.location}
                  onSuggestSelect={async value => {
                    await setFilterLocation({ value });
                    this.setState({ locationFilter: value });
                  }}
                  inputClassName={'input-search full-width'}
                  placeholder="enter location"
                  autoFocus
                />
              </div>
              <TimeDropdown className={`btn btn-grey ${s.btn}`} />
              <TransportDropdown className={`btn btn-grey ${s.btn}`} />
              <a
                href={'/question'}
                className={`btn btn-grey ${s.btn}`}
                onClick={async (e) => {
                  e.preventDefault();
                  if (filters.location && filters.time && filters.transport) {
                    const venues = await getVenues({
                      locationFilter: filters.location,
                      timeFilter: filters.time,
                      transportFilter: filters.transport,
                    });
                    setGroupId({ value: venues.groupId });
                    const questionId = await startQuiz({
                      groupId: venues.groupId,
                    });
                    setUserAsAdmin();
                    history.push(`/question/${questionId}`);
                    return true;
                  }
                  NotificationManager.error('Please fill all information');
                  return false;
                }}
              >
                <img src={logo} alt="logo" />
                Start
              </a>
            </div>
          </div>
        </section>
        <section className={`footer fixed ${s.footer}`}>
          <h2>Going with friends?</h2>
          <a
            href="/invite"
            className={`btn btn-primary ${s.btn}`}
            onClick={async e => {
              e.preventDefault();
              if (filters.location && filters.time && filters.transport) {
                const venues = await getVenues({
                  locationFilter: filters.location,
                  timeFilter: filters.time,
                  transportFilter: filters.transport,
                });
                setGroupId({ value: venues.groupId });
                await startQuiz({
                  groupId: venues.groupId,
                });
                setUserAsAdmin();
                history.push('/invite');
                return true;
              }
              NotificationManager.error('Please fill all information');
              return false;
            }}
          >
            decide together
            <i className={'icon material-icon'}>people</i>
          </a>
        </section>
      </div>
    );
  }

  renderMobile() {
    const { groupId, filters, setFilterLocation, startQuiz, setUserAsAdmin } = this.props;
    return (
      <div className={`mobile ${s.root} ${s.mobile}`}>
        <section className={`header fixed search_section ${s.search_section}`}>
          <div className={`input-search-wrapper ${s.input_wrapper}`}>
            <Geosuggest
              id="address"
              name="address"
              value={filters.location}
              onSuggestSelect={async value => {
                await setFilterLocation({ value });
                this.setState({ locationFilter: value });
              }}
              inputClassName={'input-search full-width'}
              placeholder="Start typing"
              autoFocus
            />
          </div>
          <TimeDropdown className={`btn btn-grey ${s.btn}`} />
          <TransportDropdown className={`btn btn-grey ${s.btn}`} />
        </section>
        <section className={`main full text-center ${s.main}`}>
          <a
            href="/invite"
            className={`btn btn-primary ${s.btn}`}
            onClick={async e => {
              e.preventDefault();
              if (filters.location && filters.time && filters.transport) {
                await startQuiz({
                  groupId: groupId,     // eslint-disable-line object-shorthand
                });
                setUserAsAdmin();
                history.push('/invite');
                return true;
              }
              NotificationManager.error('Please fill all information');
              return false;
            }}
          >
            add friends and decide together
            <i className={'icon material-icon'}>group</i>
          </a>
          <VenuesMap />
        </section>
        <section className={`footer fixed has-background background-normal text-center ${s.footer}`}>
          <a
            href={'/question'}
            className={`btn btn-primary ${s.btn}`}
            onClick={async (e) => {
              e.preventDefault();
              if (
                this.props.venues &&
                this.props.venues.counts > 0 &&
                !this.props.venues.isFetching
              ) {
                const questionId = await startQuiz({
                  groupId,
                });
                setUserAsAdmin();
                history.push(`/question/${questionId}`);
                return true;
              }

              NotificationManager.error('Restaurants are not loaded yet.');
              return false;
            }}
          >
            <img src={logo} alt="logo" />
            Match me to a restaurant
          </a>
        </section>
      </div>
    );
  }

  render() {
    const { isMobile } = this.props;
    if (isMobile) {
      return this.renderMobile();
    }
    return this.renderDesktop();
  }

}

const mapState = (state, props) => ({
  isMobile: state.runtime.isMobile,
  venues: selectVenues(state, props),
  filters: selectSearchFilter(state, props),
  groupId: getUserInfo(state).groupId,
});

const mapDispatch = {
  getVenues: getVenuesAction,
  setGroupId: setGroupIdAction,
  setFilterLocation: setFilterLocationAction,
  startQuiz: startQuizAction,
  setUserAsAdmin: setUserAsAdminAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Location);

export default withStyles(s)(EnhancedContent);
