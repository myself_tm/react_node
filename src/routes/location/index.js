import React from 'react';
import Layout from '../../components/Layout';
import Location from './Location';

export default {

  path: '/location',

  action() {
    return {
      component: <Layout><Location /></Layout>,
    };
  },

};
