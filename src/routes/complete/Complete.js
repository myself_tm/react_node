import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import Link from '../../components/Link';
import s from '../invite/Invite.css';

class Complete extends React.Component {
  static propTypes = {
    isMobile: PropTypes.bool,
  };

  renderDesktop = () => {
    return (
      <div className={`${s.root} ${s.complete} ${s.finished} ${s.finished}`}>
        <section className={'header has-background background-normal'}>
          <h1>All done!</h1>
        </section>
        <section className={`main full has-background background-orange text-center ${s.main} ${s.full} ${s.has_no_footer}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            <p><strong>
              We'll message you with the group's match as soon as everyone's finished.
            </strong></p>
            <p>While you wait:</p>
            <Link className={`btn btn-primary ${s.btn}`} to="/location">
              Get another match in single mode
            </Link>
          </div>
        </section>
      </div>
    );
  };

  renderMobile = () => {
    return (
      <div className={`${s.root} ${s.mobile} ${s.complete} ${s.finished}`}>
        <section className={'header has-background background-normal'}>
          <h1>All done!</h1>
        </section>
        <section className={`main full has-background background-orange text-center ${s.main} ${s.full} ${s.has_no_footer}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            <p><strong>
              We'll message you with the group's match as soon as everyone's finished.
            </strong></p>
          </div>
        </section>
        <section className={'footer fixed has-background background-image text-center'}>
          <Link className={`btn btn-primary ${s.btn}`} to="/location">
            Get another match in single mode
          </Link>
        </section>
      </div>
    );
  };

  render() {
    const { isMobile } = this.props;
    if (isMobile) {
      return this.renderMobile();
    }
    return this.renderDesktop();
  }

}

const mapState = (state) => ({
  isMobile: state.runtime.isMobile,
});

const mapDispatch = {
};

const EnhancedContent = connect(mapState, mapDispatch)(Complete);

export default withStyles(s)(EnhancedContent);
