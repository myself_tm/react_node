import React from 'react';
import Layout from '../../components/Layout';
import Complete from './Complete';

export default {

  path: '/complete',

  action() {
    return {
      component: <Layout><Complete /></Layout>,
    };
  },

};
