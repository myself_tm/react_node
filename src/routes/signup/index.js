import React from 'react';
import Layout from '../../components/Layout';
import Signup from './Signup';

export default {

  path: '/signup',

  action() {
    return {
      component: <Layout><Signup /></Layout>,
    };
  },

};
