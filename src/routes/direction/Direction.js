import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import Geosuggest from 'react-geosuggest';
import history from '../../core/history';
import TransportDropdown from '../../components/TransportDropdown';
import DirectionMap from '../../components/DirectionMap';
import { selectSearchFilter } from '../../reducers/venues';
import { setFilterLocation as setFilterLocationAction } from '../../actions/venues';
import s from '../location/Location.css';

class Direction extends React.Component {
  static propTypes = {
    filters: PropTypes.object,
    destination: PropTypes.object,
    selectSearchFilter: PropTypes.func,
    setFilterLocation: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      destination: props.destination,
      start: props.filters.location.location,
      travelMode: props.filters.transport,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      destination: nextProps.destination,
      start: nextProps.filters.location.location,
      travelMode: nextProps.filters.transport,
    });
  }

  render() {
    return (
      <div className={`mobile ${s.root} ${s.mobile} ${s.get_direction}`}>
        <section className={`header fixed search_section ${s.search_section}`}>
          <div className={`input-search-wrapper ${s.input_wrapper}`}>
            <Geosuggest
              id="address"
              name="address"
              value={this.state.destination}
              onSuggestSelect={async value => {
                this.setState({ destination: value.location });
              }}
              inputClassName={'input-search full-width'}
              placeholder="enter location"
              autoFocus
            />
          </div>
          <TransportDropdown className={`btn btn-grey ${s.btn}`} />
        </section>
        <section className={`main full text-center ${s.main}`}>
          <DirectionMap
            start={this.state.start}
            destination={this.state.destination}
            travelMode={this.state.travelMode}
          />
        </section>
        <section className={`footer fixed has-background background-normal text-center ${s.footer}`}>
          <a
            href="/result"
            className={`btn btn-primary ${s.btn}`}
            onClick={e => {
              e.prevenDefault();
              history.goBack();
            }}
          >
            {'Back to my match'}
          </a>
        </section>
      </div>
    );
  }

}

const mapState = (state, props) => ({
  filters: selectSearchFilter(state, props),
});

const mapDispatch = {
  setFilterLocation: setFilterLocationAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Direction);

export default withStyles(s)(EnhancedContent);
