import React from 'react';
import Layout from '../../components/Layout';
import Direction from './Direction';

export default {

  path: '/direction/:destination',

  action({ params }) {
    return {
      component: <Layout><Direction destination={JSON.parse(params.destination)}/></Layout>,
    };
  },

};
