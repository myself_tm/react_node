import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { NotificationManager } from 'react-notifications';
import history from '../../core/history';
import QuestionItem from '../../components/QuestionItem';
import Link from '../../components/Link';
import { selectQuestion } from '../../reducers/quiz';



import s from './Question.css';

class Question extends React.Component {
  static propTypes = {
    questionId: PropTypes.string,
    question: PropTypes.object,
  }

  constructor(props) {
    if (!props.question) {
      NotificationManager.error('Quiz is not started yet');
      history.push('/location');
    }
    super(props);
    this.state = {
      question: props.question,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        question: nextProps.question,
      });
    }
  }

  render() {
    return (
      <div className={s.root}>
        <section className={`header has-background background-normal ${s.header}`}>
          <h1>{this.state.question.question}</h1>
        </section>
        <section className={`main full has-background background-orange ${s.main}`}>
          <div className={`answers-container text-center ${s.answers_container}`}>
            {
              this.state.question.answers.map((answer, $index) => {
                return (
                  <QuestionItem
                    key={$index}
                    questionId={this.state.question.questionId}
                    imgUrl={this.state.question.answerImages[$index]}
                    text={answer}
                  />
                );
              })
            }
          </div>
        </section>
        <section className={'footer fixed has-background background-image text-center'}>
          <Link className={'btn btn-primary'} to="/invite">
            add friends and decide together
            <i className={'icon material-icon'}>group</i>
          </Link>
        </section>
      </div>
    );
  }

}

const mapState = (state, props) => ({
  question: selectQuestion(state, props.questionId),
});

const mapDispatch = {
};

const EnhancedContent = connect(mapState, mapDispatch)(Question);

export default withStyles(s)(EnhancedContent);
