import React from 'react';
import Layout from '../../components/Layout';
import Question from './Question';
import { isQuizStarted } from '../../reducers/quiz';

export default {

  path: ['/question/:questionId', '/question'],

  async action({ store, params }) {
    const state = store.getState();
    const isStarted = isQuizStarted(state, params.questionId);
    if (!isStarted) {
      return { redirect: '/type' };
    }

    return {
      component: <Layout><Question questionId={params.questionId} /></Layout>,
    };
  },

};
