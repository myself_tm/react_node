export default {

  path: '/',

  action() {
    return { redirect: '/login' };
  },

};
