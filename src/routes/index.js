/* eslint-disable global-require */

// The top-level (parent) route
export default {

  path: '/',

  // Keep in mind, routes are evaluated in order
  children: [
    require('./home').default,
    require('./contact').default,
    require('./login').default,
    require('./signup').default,
    require('./type').default,
    require('./location').default,
    require('./question').default,
    require('./result').default,
    require('./direction').default,
    require('./invite').default,
    require('./join').default,
    require('./sent').default,
    require('./phone').default,
    require('./complete').default,
    require('./admin').default,

    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    require('./content').default,
    require('./notFound').default,
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();

    // Provide default values for title, description etc.
    route.title = 'Yazabi';
    route.description = route.description || '';

    return route;
  },

};
