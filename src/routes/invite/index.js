import React from 'react';
import Layout from '../../components/Layout';
import Invite from './Invite';

export default {

  path: '/invite',

  action({ store }) {
    const state = store.getState();
    if (!state.user.userId || !state.user.groupId) {
      return { redirect: '/' };
    }
    return {
      component: <Layout><Invite /></Layout>,
    };
  },

};
