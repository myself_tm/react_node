import React, { PropTypes } from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications';
import history from '../../core/history';
import {
  addMember as addMemberAction,
  updateMember as updateMemberAction,
  savePhone as savePhoneAction,
  loadPhone as loadPhoneAction,
  sendMessage as sendMessageAction,
} from '../../actions/group';
import { setUserAsAdmin as setUserAsAdminAction } from '../../actions/user';
import { setQuizTypeAsGroup as setQuizTypeAsGroupAction } from '../../actions/quiz';
import { selectSearchFilter } from '../../reducers/venues';
import { selectUsers } from '../../reducers/group';
import s from './Invite.css';

class Invite extends React.Component {
  static propTypes = {
    groupId: PropTypes.string,
    users: PropTypes.array,
    filters: PropTypes.object,
    hostUrl: PropTypes.string,
    selectSearchFilter: PropTypes.func,
    addMember: PropTypes.func,
    updateMember: PropTypes.func,
    setUserAsAdmin: PropTypes.func,
    setQuizTypeAsGroup: PropTypes.func,
    savePhone: PropTypes.func,
    loadPhone: PropTypes.func,
    sendMessage: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      users: props.users,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        users: this.props.users,
      });
    }
  }

  render() {
    const { groupId, users, hostUrl, addMember, updateMember, setUserAsAdmin, setQuizTypeAsGroup, sendMessage, savePhone } = this.props;      // eslint-disable-line max-len
    let newPhone;
    return (
      <div className={s.root}>
        <section className={'header has-background background-normal'}>
          <h1>Add your friends:</h1>
          <h1><strong>Decide as a group</strong></h1>
        </section>
        <section className={`main full has-background background-orange ${s.main} ${s.full}`}>
          <img src={require('../../public/background-content.png')} alt="overlay" className={s.overlay_image} />
          <div className={`text-center ${s.container}`}>
            {
              users.map((user, $index) => {
                return (
                  <input
                    key={$index}
                    id={$index}
                    type="tel"
                    name={`phone${$index}`}
                    onBlur={e => {
                      updateMember({ userInd: $index, phoneNo: e.target.value });
                    }}
                    className={`input-search text-center ${s.input_search}`}
                    placeholder="Type a phone number"
                    autoFocus
                    defaultValue={user}
                  />
                );
              })
            }
            <input
              id="new_phone_number"
              type="tel"
              name="new_phone"
              ref={e => (newPhone = e)}
              className={`input-search text-center ${s.input_search}`}
              placeholder="Type a phone number"
              autoFocus
            />

            <button
              href={'#'}
              className={`btn btn-primary ${s.btn}`}
              onClick={(e) => {
                e.preventDefault();
                if (newPhone.value && newPhone.value.length > 0) {
                  addMember({ phoneNo: newPhone.value });
                  newPhone.value = '';
                }
              }}
            >
              add more
              <i className={`icon material-icon ${s.icon}`}>person_add</i>
            </button>
          </div>
        </section>
        <section className={`footer fixed has-background background-image text-center ${s.footer}`}>
          <button
            className={`btn btn-primary ${s.btn}`}
            onClick={async e => {
              e.preventDefault();
              try {
                const phoneNumbers = users;
                if (newPhone.value && newPhone.value.length > 0) {
                  phoneNumbers.push(newPhone.value);
                }
                const { usersWithPhone } = await savePhone({ groupId, phoneNumbers });
                setUserAsAdmin();
                setQuizTypeAsGroup();

                for (let index = 0; index < usersWithPhone.length; index += 1) {
                  if (usersWithPhone[index]) {
                    const filters = JSON.stringify({
                      location: this.props.filters.location.location,
                      time: this.props.filters.time.value,
                      transport: this.props.filters.transport.unit,
                    });
                    const url = `${hostUrl}/join/${groupId}/${usersWithPhone[index][0]}/${filters}`;
                    const msg = `Someone has invited you to an event on Yazabi! Follow this link to join:%0a${url}`;
                    await sendMessage({ phoneNumber: usersWithPhone[index][1], msg });
                  }
                }
                history.push('/sent');
              } catch (err) {
                NotificationManager.error('Can\'t send SMS to those phone numbers');
              }
            }}
          >
            Send Invitations
            <i className={`icon material-icon ${s.icon}`}>tag_faces</i>
          </button>
        </section>
      </div>
    );
  }
}

const mapState = (state, props) => ({
  groupId: state.user.groupId,
  users: selectUsers(state, props),
  filters: selectSearchFilter(state),
  hostUrl: state.runtime.host,
});

const mapDispatch = {
  addMember: addMemberAction,
  updateMember: updateMemberAction,
  setUserAsAdmin: setUserAsAdminAction,
  setQuizTypeAsGroup: setQuizTypeAsGroupAction,
  savePhone: savePhoneAction,
  loadPhone: loadPhoneAction,
  sendMessage: sendMessageAction,
};

const EnhancedContent = connect(mapState, mapDispatch)(Invite);

export default withStyles(s)(EnhancedContent);
