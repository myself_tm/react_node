/* eslint-disable import/prefer-default-export */

export const SET_RUNTIME_VARIABLE = 'SET_RUNTIME_VARIABLE';
export const SET_LOCALE_START = 'SET_LOCALE_START';
export const SET_LOCALE_SUCCESS = 'SET_LOCALE_SUCCESS';
export const SET_LOCALE_ERROR = 'SET_LOCALE_ERROR';
export const FETCH_CONTENT_START = 'FETCH_CONTENT_START';
export const FETCH_CONTENT_SUCCESS = 'FETCH_CONTENT_SUCCESS';
export const FETCH_CONTENT_ERROR = 'FETCH_CONTENT_ERROR';

export const FETCH_VENUES_START = 'FETCH_VENUES_START';
export const FETCH_VENUES_SUCCESS = 'FETCH_VENUES_SUCCESS';
export const FETCH_VENUES_FAILURE = 'FETCH_VENUES_FAILURE';

export const SET_FILTER_LOCATION = 'SET_FILTER_LOCATION';
export const SET_FILTER_TRANSPORT = 'SET_FILTER_TRANSPORT';
export const SET_FILTER_TIME = 'SET_FILTER_TIME';

export const SET_QUIZ_TYPE = 'SET_QUIZ_TYPE';

export const FETCH_QUIZ_START = 'FETCH_QUIZ_START';
export const FETCH_QUIZ_SUCCESS = 'FETCH_QUIZ_SUCCESS';
export const FETCH_QUIZ_FAILURE = 'FETCH_QUIZ_FAILURE';

export const JOIN_QUIZ_START = 'JOIN_QUIZ_START';
export const JOIN_QUIZ_SUCCESS = 'JOIN_QUIZ_SUCCESS';
export const JOIN_QUIZ_FAILURE = 'JOIN_QUIZ_FAILURE';

export const SET_ANSWER_START = 'SET_ANSWER_START';
export const SET_ANSWER_SUCCESS = 'SET_ANSWER_SUCCESS';
export const SET_ANSWER_FAILURE = 'SET_ANSWER_FAILURE';

export const QUIZ_SUCCESS = 'QUIZ_SUCCESS';

export const FETCH_RESULT_START = 'FETCH_RESULT_START';
export const FETCH_RESULT_SUCCESS = 'FETCH_RESULT_SUCCESS';
export const FETCH_RESULT_FAILURE = 'FETCH_RESULT_FAILURE';

export const FETCH_DIRECTION_START = 'FETCH_DIRECTION_START';
export const FETCH_DIRECTION_SUCCESS = 'FETCH_DIRECTION_SUCCESS';
export const FETCH_DIRECTION_FAILURE = 'FETCH_DIRECTION_FAILURE';

export const GROUP_ADD_MEMBER = 'ADD_MEMBER';
export const GROUP_UPDATE_MEMBER = 'EDIT_MEMBER';
export const GROUP_SEND_INVITATION_START = 'ADD_MEMBER_START';
export const GROUP_SEND_INVITATION_SUCCESS = 'ADD_MEMBER_SUCCESS';
export const GROUP_SEND_INVITATION_FAILURE = 'ADD_MEMBER_FAILURE';

export const SET_PHONES_START = 'SET_PHONES_START';
export const SET_PHONES_SUCCESS = 'SET_PHONES_SUCCESS';
export const SET_PHONES_FAILURE = 'SET_PHONES_FAILURE';

export const FETCH_PHONES_START = 'FETCH_PHONES_START';
export const FETCH_PHONES_SUCCESS = 'FETCH_PHONES_SUCCESS';
export const FETCH_PHONES_FAILURE = 'FETCH_PHONES_FAILURE';

export const SEND_SMS_START = 'SEND_SMS_START';
export const SEND_SMS_SUCCESS = 'SEND_SMS_SUCCESS';
export const SEND_SMS_FAILURE = 'SEND_SMS_FAILURE';

export const SET_USER_ID = 'SET_USER_ID';
export const SET_GROUP_ID = 'SET_GROUP_ID';
export const SET_USER_ADMIN = 'SET_USER_ADMIN';
export const SET_USER_INVITEE = 'SET_USER_INVITEE';
export const SET_USER_LOCATION = 'SET_USER_LOCATION';
