/* eslint-disable import/prefer-default-export */

export const GROUP_ID = '4601_1474924830018';
export const USER_ID = '57e97e06b8d7cb7c188ebcfe';

export const CONVRT_RATE = {
  car: 8,
  bike: 4,
  walk: 0.9,
};

export const QUIZ_TYPE_SINGLE = 'SINGLE';
export const QUIZ_TYPE_GROUP = 'GROUP';

export const TIME_OPTIONS = [
  {
    value: 5,
    unit: 'min',
    label: '5 min',
  },
  {
    value: 15,
    unit: 'min',
    label: '15 min',
  },
  {
    value: 30,
    unit: 'min',
    label: '30 min',
  },
];
export const TRANSPORT_OPTIONS = [
  {
    value: 'walk',
    icon: 'directions_walk',
    unit: 0.9,
  },
  {
    value: 'bike',
    icon: 'directions_bike',
    unit: 4,
  },
  {
    value: 'car',
    icon: 'directions_car',
    unit: 8,
  },
];
